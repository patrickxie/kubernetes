# Kubernetes

From https://github.com/kubernetes/kubernetes. We made the following modifications based on the official kubernetes.

## kube-apiserver
* Add NsHostPathAndPrivilege admission plugin. ([code](plugin/pkg/admission/nshostpathprivilege/admission.go))


## kube-controller-manager
* Add NodePVController for timeout hostpathpv clean.([code](/pkg/controller/nodepv/nodepvcontroller.go))
* Modify the sync logic of RS and RC(working with kube-rescheduler).([code](pkg/controller/replicaset/replica_set.go#lines-572))

## kube-scheduler

- Add some predicate.
  	- NodeNameSpacePredicate: Effective at algorithm-provider=NodeNameSpacePredicate.
  	- PodHostPathPVDiskPressure
  	- HostPathPVAffinityPredicate
  	- HostPathPVAffinityPredicate2
  	- NamespacesNodeSelector

- Add some priority.
  	- NodeNameSpacePriority: Effective at algorithm-provider=NodeNameSpacePriority.
  	- PVNodeSpreadPriority
  	- NodePVDiskUsePriority

## kube-rescheduler
The rescheudler module is base of https://github.com/kubernetes/contrib/tree/master/rescheduler. Added a namespace isolation scheduling feature on officail basis. ([code](cmd/kube-rescheduler/rescheduler.go))

## kubelet
* Add local disk quota management module used in hostpath pv + pvc. ([code](pkg/kubelet/diskquota/xfs/xfsquotamanager.go))
* Made some changes to cadvisor such as adding kafka and metrics clustername for multi-cluster. storage.([code](pkg/kubelet/cadvisor/cadvisor_linux.go))
* Pod add annotation to turn on or off the svc information in the same namespace kept in pod env.([code](pkg/kubelet/kubelet_pods.go))

